# Testy do zadania 1

Przed pierwszym uruchomieniem: `pip install -r requirements.txt` (trzeba mieć `python-pip`/`python3-pip`).

W celach testowania na studentsie trzeba wybrać odpowiednią wartość stałej RUN_ON_STUDENTS w pliku server_wrap.py.

Użycie: `python3 test.py [opcjonalnie: --debug]`

### Docs

Przypominam, by nie zadawać pytań na forum jak nie trzeba. 
Istnieje docs z listą pytań i odpowiedzi.
Link do niego jest gdzieś na grupie fb.

### Dodawanie swoich testów

Patrz [tutaj](https://gitlab.com/mimuw-ipp-2021/testy-duze-zadanie-3)
