from test_commandline_params import test_commandline_params
from test_small_correctness import test_small_correctness
from test_big_correctness import test_big_correctness
from test_limits import test_limits
from test_reservation_timing_out import test_reservation_timing_out
from server_wrap import RUN_ON_STUDENTS
import os

if __name__ == '__main__':
    print("\n[Reklama] https://forms.gle/4WTiFSM1xQDfhfiW9 \n")
    print("Każdy test powinien działać poniżej 20 sekund.")

    if RUN_ON_STUDENTS:
        print("Test test_commandline_params jest pominięty")
        print("Aby uruchomić testy inaczej, zmień wartość stałej w pliku server_wrap.py")


    tests = [
        test_small_correctness,
        test_big_correctness,
        test_limits,
        test_reservation_timing_out,
    ]

    if not RUN_ON_STUDENTS:
        tests.append(test_commandline_params)

    try:
        for t in tests:
            print(t.__name__ + '...')
            t()
        print('Tests passed!')
    except Exception as ex:
        os.system("pkill ticket_server")
        raise ex
        
